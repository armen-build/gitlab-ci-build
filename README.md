# Gitlab Builds

O Gitlab permite que o CI inclua arquivos de outros projetos através da diretiva `include`.

Esse projeto contém as builds para os tipos de projetos Armen.

## Java Microservices

Para projetos de microserviço Java.

Váriaveis:

| Variavel   | Valor padrão | Descrição |
| ---------- | ------------ | --------- |
| JDK_VERSION | jdk-10 | Qual JDK utilizar, valores possíveis `jdk-10` e `jdk-8` |
| AZURE_CI_REGISTRY_IMAGE | - | Nome da imagem docker para subir na Azure |
| CODE_COVERAGE_REPORT_PATH | target/site/jacoco/index.html | Path for the code coverage report |
```yaml
---
variables:
  # Nome da imagem na Azure
  AZURE_CI_REGISTRY_IMAGE: armen.azurecr.io/armen/api/armen-execution-service

include: https://gitlab.com/armen-build/gitlab-ci-build/raw/master/.gitlab-ci-java-microservice.yaml
```

## Java API

Para projetos Java que são APIs.

Váriaveis:

| Variavel   | Valor padrão | Descrição |
| ---------- | ------------ | --------- |
| JDK_VERSION | jdk-10 | Qual JDK utilizar, valores possíveis `jdk-10` e `jdk-8` | MULTI_MODULE_PROJECT | FALSE | Flag que indica que é um projeto de módulos do Maven |
| CODE_COVERAGE_REPORT_PATH | target/site/jacoco/index.html | Caminho do relatório do code coverage |

```yaml
---
variables:

include: https://gitlab.com/armen-build/gitlab-ci-build/raw/master/.gitlab-ci-java.yaml
```